# boutique_back

This application is generated using [LoopBack 4 CLI](https://loopback.io/doc/en/lb4/Command-line-interface.html) with the
[initial project layout](https://loopback.io/doc/en/lb4/Loopback-application-layout.html).

## Lancer le projet

Pour installer les dépendances:
```sh
npm install
```

Pour lancer le build:
```sh
npm run build
```

Pour lancer la construction de la BDD:
```sh
npm run migrate
```

Pour lancer le service:
```sh
npm start
```

### Les requêtes
Il est possible de faire des requêtes sur tous les controllers (src/controllers).

- Relations
Pour afficher les relations dans la réponse il faut jouter ```?filter[include][0][relation]=categories``` dans la requête.
0 est la position. Donc si on a plusieurs relations, il faut incrémenter.
categories est le nom de modèle que l'on souhaite avoir. Dans ce cas précis on souhaite afficher les informations relatives à la catégorie.

- Insomnia
Vous pouvez vous servir d'Insomnia pour tester les requêtes.
Le fichier insomnia joint à la racine du projet vous permet d'importer les requêtes déjà faites pour tester les réponses.

## Install dependencies

By default, dependencies were installed when this application was generated.
Whenever dependencies in `package.json` are changed, run the following command:

```sh
npm install
```

To only install resolved dependencies in `package-lock.json`:

```sh
npm ci
```

## Run the application

```sh
npm start
```

You can also run `node .` to skip the build step.

Open http://127.0.0.1:3000 in your browser.

## Rebuild the project

To incrementally build the project:

```sh
npm run build
```

To force a full build by cleaning up cached artifacts:

```sh
npm run rebuild
```

## Fix code style and formatting issues

```sh
npm run lint
```

To automatically fix such issues:

```sh
npm run lint:fix
```

## Other useful commands

- `npm run migrate`: Migrate database schemas for models
- `npm run openapi-spec`: Generate OpenAPI spec into a file
- `npm run docker:build`: Build a Docker image for this application
- `npm run docker:run`: Run this application inside a Docker container

## Tests

```sh
npm test
```

## What's next

Please check out [LoopBack 4 documentation](https://loopback.io/doc/en/lb4/) to
understand how you can continue to add features to this application.

[![LoopBack](https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png)](http://loopback.io/)
